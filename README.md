
<h1>
 # Keylogger-for-Windows
</h1>
<h2>
 Simple keylogger, keyboard captures.
</h2>
<p>
The program was made in C language using DEV-C ++
main purpose: to capture keyboard entries such as letters, numbers and spaces, and thus print them in a .txt doc.
</p>
<p>
The project is still in its early stages, so there are still defects to be fixed, 'Backspace' currently used [<-]
later I want to edit the log to delete the character. Several special characters are recognized as numeric characters as well ('!' = '1', '@' = '2', '#' = '3' ...).
</p>
<p>
 NOTE:
</br>
If you want the program to capture background entries, simply delete the comment bars on line # 10.
</br>
If you want the program to run every time the machine starts, remove the comment bars on line # 22.
</p>
